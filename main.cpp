#include <iostream>
#include <vector>
#include <random>
#include <chrono>
#include <algorithm>

using namespace std;

/**
 * Rock Paper Scissors Lizard Spock Program
 * (because it's a challenge and I like lizards; not because of TBBT)
 * by Sean Castillo 2019
 */

 const std::vector<string> choices = {"rock", "paper", "scissors", "lizard", "spock"};

 std::string getChoiceCPU()
 {
     // seed as demonstrated on stack overflow and cplusplus.com
     unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
     std::default_random_engine generator (seed);
     std::uniform_int_distribution<int> distribution(0,4);
     return choices[distribution(generator)];
 }

 /** returns 0: cpu wins, 1: player wins, 2: tie */
 unsigned int determineWinner(string choicePlayer, string choiceCPU)
 {
    //TODO: convert inputs to lower case

    if (choicePlayer == "lizard"  )
    {
        if      (choiceCPU == "rock"  || choiceCPU == "scissors")  { return 0; }
        else if (choiceCPU == "paper" || choiceCPU == "spock"   )  { return 1; }
        return 2;
    }

    if (choicePlayer == "spock"   )
    {
        if      (choiceCPU == "lizard"   || choiceCPU == "paper")  { return 0; }
        else if (choiceCPU == "scissors" || choiceCPU == "rock" )  { return 1; }
        return 2;
    }

    if (choicePlayer == "scissors")
    {
        if      (choiceCPU == "spock" || choiceCPU == "rock"    )  { return 0; }
        else if (choiceCPU == "paper" || choiceCPU == "lizard"  )  { return 1; }
        return 2;
    }

    if (choicePlayer == "paper"   )
    {
        if      (choiceCPU == "scissors" || choiceCPU == "lizard") { return 0; }
        else if (choiceCPU == "spock"    || choiceCPU == "rock"  ) { return 1; }
        return 2;
    }
    if (choicePlayer == "rock"    )
    {
        if      (choiceCPU == "spock"    || choiceCPU == "paper" ) { return 0; }
        else if (choiceCPU == "scissors" || choiceCPU == "lizard") { return 1; }
        return 2;
    }
 }

int main()
{
    unsigned int winner;
    string choicePlayer;
    while (1)
    {
        chooseLabel:
        cout << "Rock! Paper! Scissors! Lizard! Spock!" << endl;
        string choiceCPU = getChoiceCPU();
        cin >> choicePlayer;

        // convert input to lower case
        std::for_each(choicePlayer.begin(), choicePlayer.end(), [](char & c)
            {   c = ::tolower(c); });

        // check for exit
        if (choicePlayer == "exit")
        {
            cout << "Stopping the game." << endl;
            return 0;
        }

        // check input for validity
        if (std::find(choices.begin(), choices.end(), choicePlayer) != choices.end() == false)
        {
            cout << "Not a valid choice! Try again!" << endl;
            cin.clear();
            cin.ignore(100000, '\n');
            goto chooseLabel;
        }

        cout << "You chose " << choicePlayer << endl;
        cout << "CPU chose " << choiceCPU    << endl;

        winner = determineWinner(choicePlayer, choiceCPU);
        if      (winner == 0) { cout << "CPU wins!" << endl; }
        else if (winner == 1) { cout << "You win!"  << endl; }
        else                  { cout << "Tie!"      << endl; }
    }
    return 0;
}
